import React from 'react';


export default class FinishExerciseForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      exercise: this.props.exercise,
      exerciseReps: this.props.exerciseReps,
      exerciseWeight: this.props.exerciseWeight,
    }
  }
  onExerciseChange = (e) => {
    const exercise = e.target.value.trim();
    this.setState(() => ({ exercise }));
  }
  onExerciseRepsChange = (e) => {
    const exerciseReps = e.target.value.trim();
    this.setState(() => ({ exerciseReps }));
  }
  onExerciseWeightChange = (e) => {
    const exerciseWeight = e.target.value.trim();
    this.setState(() => ({ exerciseWeight }));
  }
  submitFormFinishExercise = (e) => {
    e.preventDefault();
    const exerciseValue = this.state.exercise;
    const exerciseRepsValue = this.state.exerciseReps;
    const exerciseWeight = this.state.exerciseWeight;

    this.props.handleFinishExercise(exerciseValue, exerciseRepsValue, exerciseWeight);
  }
  render() {
    return (
        <div>
          <form onSubmit={this.submitFormFinishExercise}>
            <input
              type="text"
              name="exercise"
              value={this.state.exercise}
              onChange={this.onExerciseChange}
              placeholder="Exercise Name"
            />
            <input
              type="number"
              name="exerciseReps"
              value={this.state.exerciseReps}
              onChange={this.onExerciseRepsChange}
              placeholder="Reps"
            />
            <input
              type="number"
              name="exerciseWeight"
              value={this.state.exerciseWeight}
              onChange={this.onExerciseWeightChange}
              placeholder="Weight"
            />
            <button>Finish Exercise</button>
          </form>
        </div>
    );
  }
}