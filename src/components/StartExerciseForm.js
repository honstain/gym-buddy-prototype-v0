import React from 'react';


export default class StartExerciseForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      exercise: '',
      exerciseReps: '',
      exerciseWeight: '',
    }
  }
  onQuickExerciseSelect = (exercise) => {
    this.setState(() => ({ exercise }));
  }
  onExerciseChange = (e) => {
    const exercise = e.target.value.trim();
    this.setState(() => ({ exercise }));
  }
  onExerciseRepsChange = (e) => {
    const exerciseReps = e.target.value.trim();
    this.setState(() => ({ exerciseReps }));
  }
  onExerciseWeightChange = (e) => {
    const exerciseWeight = e.target.value.trim();
    this.setState(() => ({ exerciseWeight }));
  }
  submitFormStartExercise = (e) => {
    e.preventDefault();
    const exerciseValue = this.state.exercise;
    const exerciseRepsValue = this.state.exerciseReps;
    const exerciseWeight = this.state.exerciseWeight;

    this.props.handleStartExercise(exerciseValue, exerciseRepsValue, exerciseWeight);
  }
  render() {
    return (
        <div>
          <form onSubmit={this.submitFormStartExercise}>
            <input
              type="text"
              name="exercise"
              value={this.state.exercise}
              onChange={this.onExerciseChange}
              placeholder="Exercise Name"
            />
            <input
              type="number"
              name="exerciseReps"
              value={this.state.exerciseReps}
              onChange={this.onExerciseRepsChange}
              placeholder="Reps"
            />
            <input
              type="number"
              name="exerciseWeight"
              value={this.state.exerciseWeight}
              onChange={this.onExerciseWeightChange}
              placeholder="Weight"
            />
            <button>Start Exercise</button>
          </form>
          { this.props.suggestedExercises.length > 0 && <p>Suggested Exercises</p> }
          {
            this.props.suggestedExercises.map((exercise, index) => (
              <button
                key={index}
                onClick={() => this.onQuickExerciseSelect(exercise)}
              >
                {exercise}
              </button>
            ))
          }
        </div>
    );
  }
}
