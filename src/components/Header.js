import React from 'react';

const Header = (props) => (
  <div className="header">
    <h1 className="header__title">Gym Buddy</h1>
    <p className="header__title">Prototype V0</p>
  </div>
);

export default Header;