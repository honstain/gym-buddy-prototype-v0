import React from 'react';
import moment from 'moment';

import StartExerciseForm from './StartExerciseForm';
import FinishExerciseForm from './FinishExerciseForm';
import { Timer1 } from '../utils';



export default class WorkoutStates extends React.Component {
  state = {
    sessionStarted: false,
    beginTime: undefined,
    exerciseInProgress: false,
    exerciseName: undefined,
    exerciseReps: undefined,
    exerciseWeight: undefined,
    exercises: [],
    suggestedExercises: ['Pullup','Bench','Squat','Seated Row'],
  }
  handleStartWorkout = () => {
    const beginTime = this.state.beginTime;
    this.setState(() => ({
      sessionStarted: true,
      beginTime: beginTime ? beginTime: moment().valueOf(),
    }));
  }
  handleStartExercise = (exerciseValue, exerciseRepsValue, exerciseWeight) => {
    const prevBeginTime = this.state.beginTime;
    const newBeginTime = moment().valueOf();

    console.log(`startExercise ${exerciseValue} start:${moment(prevBeginTime).format()} finish:${moment(newBeginTime).format()}`);

    this.setState((prevState) => ({
      exerciseInProgress: true,
      exerciseName: exerciseValue,
      exerciseReps: exerciseRepsValue,
      exerciseWeight: exerciseWeight,
      beginTime: newBeginTime,
      exercises: prevState.exercises.concat({
        exerciseName: 'Rest',
        exerciseReps: 0,
        exerciseWeight: 0,
        startTime: prevBeginTime,
        finishTime: newBeginTime,
      })
    }));
  }
  handleFinishExercise = (exerciseValue, exerciseRepsValue, exerciseWeight) => {
    const startTime = this.state.beginTime;
    const finishTime = moment().valueOf();

    console.log(`finishExercise ${this.state.exerciseName} ${exerciseValue} start:${moment(this.state.beginTime).format()} finish:${moment(finishTime).format()}`);

    this.setState((prevState) => ({
      exerciseInProgress: false,
      exerciseName: undefined,
      exerciseReps: undefined,
      exerciseWeight: undefined,
      beginTime: moment().valueOf(),
      exercises: prevState.exercises.concat({
        exerciseName: exerciseValue,
        exerciseReps: exerciseRepsValue,
        exerciseWeight: exerciseWeight,
        startTime: startTime,
        finishTime: finishTime,
      })
    }));
  }
  formatTimeDifference = (startTime, finishTime) => {
    const start = moment(startTime);
    const finish = moment(finishTime);
    // https://stackoverflow.com/questions/13262621/how-do-i-use-format-on-a-moment-js-duration
    const diff = finish.diff(start);
    const dur = moment.utc(diff).format("m:ss");
    //console.log(`${start.format()} ${finish.format()} ${diff} ${dur}`);
    return dur;
  };
  componentDidMount() {
    try {
      const json = localStorage.getItem('exercises');
      const exercises = JSON.parse(json);

      if (exercises) {
        const lastFinishTime = exercises.slice(-1)[0].finishTime;
        console.log(moment(lastFinishTime).format());
        this.setState(() => ({ beginTime: lastFinishTime, exercises: exercises }));
      }
    } catch (e) {
      // Do nothing
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevState.exercises.length !== this.state.exercises.length) {
      const json = JSON.stringify(this.state.exercises);
      localStorage.setItem('exercises', json);
    }
  }
  render() {
    return (
      <div className="workoutstates">
        <div>
          <ul>
            {
              this.state.exercises.map((exerciseRecord, index) => (
                <li key={index}>
                  {exerciseRecord.exerciseName} Reps:{exerciseRecord.exerciseReps} Weight:{exerciseRecord.exerciseWeight} -
                  Start: {moment(exerciseRecord.startTime).format("HH:mm:ss")} -
                  Finished: {moment(exerciseRecord.finishTime).format("HH:mm:ss")} -
                  Duration: {this.formatTimeDifference(exerciseRecord.startTime, exerciseRecord.finishTime)}
                </li>
              ))
            }
          </ul>
        </div>

        {/* State 0: Start workout */}
        {!this.state.sessionStarted &&
          <button onClick={this.handleStartWorkout}>Start Workout</button>
        }

        {/* State 1: Start exercise */}
        {this.state.sessionStarted && !this.state.exerciseInProgress &&
          <div>
            {this.state.exercises.length === 0 ?
              <p>Workout started at {moment(this.state.beginTime).format("HH:mm:ss")}</p>
              :
              <p>Last exercise finished at {moment(this.state.beginTime).format("HH:mm:ss")}</p>
            }

            <Timer1 startTime={this.state.beginTime}/>

            <StartExerciseForm
              handleStartExercise={this.handleStartExercise}
              exerciseName={this.state.exerciseName}
              suggestedExercises={this.state.suggestedExercises}
            />
          </div>
        }

        {/* State 2: Finish exercise */}
        {this.state.sessionStarted && this.state.exerciseInProgress &&
          <div>
            <p>Exercise started at {moment(this.state.beginTime).format("HH:mm:ss")}</p>
            <p>Exercise name {this.state.exerciseName}</p>
            <p>Exercise Reps {this.state.exerciseReps}</p>
            <p>Exercise Weight {this.state.exerciseWeight}</p>

            <FinishExerciseForm
              handleFinishExercise={this.handleFinishExercise}
              exercise={this.state.exerciseName}
              exerciseReps={this.state.exerciseReps}
              exerciseWeight={this.state.exerciseWeight}
            />
          </div>
        }
      </div>
    );
  }
}

