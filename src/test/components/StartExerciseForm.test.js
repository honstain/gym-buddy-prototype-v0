import React from 'react';
import { shallow } from 'enzyme';
import toJSON from 'enzyme-to-json';
import StartExerciseForm from '../../components/StartExerciseForm';


test('should render StartExercise form', () => {
  const wrapper = shallow(
    <StartExerciseForm
      handleStartExercise={jest.fn()}
      exerciseName={undefined}
      suggestedExercises={[]}
    />
  );
  expect(toJSON(wrapper)).toMatchSnapshot();
});

test('should start exercise on form submit', () => {
  const handleStartExerciseSpy = jest.fn();
  const wrapper = shallow(
    <StartExerciseForm
      handleStartExercise={ handleStartExerciseSpy }
      exerciseName={undefined}
      suggestedExercises={[]}
    />
  );

  wrapper.find('input').at(0).simulate('change', { target: { value: 'Bench' }});
  wrapper.find('input').at(1).simulate('change', { target: { value: '10' }});
  wrapper.find('input').at(2).simulate('change', { target: { value: '150' }});

  wrapper.find('form').simulate('submit', {
    preventDefault: () => {},
  });
  expect(handleStartExerciseSpy).toHaveBeenLastCalledWith('Bench', '10', '150');
});

test('clicking a suggested exercise should update the state', () => {
  const handleStartExerciseSpy = jest.fn();
  const wrapper = shallow(
    <StartExerciseForm
      handleStartExuercise={ handleStartExerciseSpy }
      exerciseName={undefined}
      suggestedExercises={['Bench']}
    />
  );

  wrapper.find('button').at(1).simulate('click');
  expect(wrapper.state('exercise')).toEqual('Bench');
});