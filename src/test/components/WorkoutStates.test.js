import React from 'react';
import { shallow } from 'enzyme';
import toJSON from 'enzyme-to-json';
import WorkoutStates from '../../components/WorkoutStates';


test('should render WorkoutStates', () => {
  const wrapper = shallow(<WorkoutStates />);
  expect(toJSON(wrapper)).toMatchSnapshot();
});

test('should render WorkoutStates on start workout', () => {
  const dateNow = '2019-06-05T00:00:00.000Z';
  jest.spyOn(global.Date, 'now')
    .mockImplementationOnce(() => new Date(dateNow).valueOf());

  const wrapper = shallow(<WorkoutStates />);

  const button = wrapper.find('button').at(0);
  button.simulate('click');

  expect(toJSON(wrapper)).toMatchSnapshot();
  expect(wrapper.state('exerciseInProgress')).toBe(false);
});

test('click through all of the application states - start workout, start exercise, finish exercise', () => {
  // const dateNow = '2019-06-05T00:00:00.000Z';
  // jest.spyOn(global.Date, 'now')
  //   .mockImplementation(() => new Date(dateNow).valueOf());

  const wrapper = shallow(<WorkoutStates />);

  jest.spyOn(global.Date, 'now')
    .mockImplementationOnce(() => new Date('2019-06-05T00:00:00.000Z').valueOf());

  const button = wrapper.find('button').at(0);
  button.simulate('click');
  expect(toJSON(wrapper)).toMatchSnapshot();
  expect(wrapper.state('exerciseInProgress')).toBe(false);

  jest.spyOn(global.Date, 'now')
    .mockImplementationOnce(() => new Date('2019-06-05T00:01:00.000Z').valueOf());

  wrapper.find('StartExerciseForm').prop('handleStartExercise')('Bench', '10');
  expect(toJSON(wrapper)).toMatchSnapshot();
  expect(wrapper.state('exerciseInProgress')).toBe(true);

  jest.spyOn(global.Date, 'now')
    .mockImplementationOnce(() => new Date('2019-06-05T00:02:00.000Z').valueOf())
    .mockImplementationOnce(() => new Date('2019-06-05T00:02:01.000Z').valueOf());

  wrapper.find('FinishExerciseForm').prop('handleFinishExercise')('Bench', '10');
  expect(toJSON(wrapper)).toMatchSnapshot();
  expect(wrapper.state('exerciseInProgress')).toBe(false);
});