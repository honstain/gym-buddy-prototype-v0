import React from 'react';
import { shallow } from 'enzyme';
import toJSON from 'enzyme-to-json';
import FinishExerciseForm from '../../components/FinishExerciseForm';


test('should render FinishExercise form', () => {
  const wrapper = shallow(
    <FinishExerciseForm
      handleFinishExercise={jest.fn()}
      exerciseName={undefined}
      suggestedExercises={[]}
    />
  );
  expect(toJSON(wrapper)).toMatchSnapshot();
});

test('should finish exercise on form submit', () => {
  const handleFinishExerciseSpy = jest.fn();
  const wrapper = shallow(
    <FinishExerciseForm
      handleFinishExercise={ handleFinishExerciseSpy }
      exercise={'Bench'}
      exerciseReps={'10'}
      exerciseWeight={'150'}
    />
  );
  wrapper.find('form').simulate('submit', {
    preventDefault: () => {},
  });
  expect(handleFinishExerciseSpy).toHaveBeenLastCalledWith('Bench', '10', '150');
});

test('should finish exercise on form submit with updates', () => {
  const handleFinishExerciseSpy = jest.fn();
  const wrapper = shallow(
    <FinishExerciseForm
      handleFinishExercise={ handleFinishExerciseSpy }
      exercise={'Bench'}
      exerciseReps={'10'}
    />
  );

  wrapper.find('input').at(0).simulate('change', { target: { value: 'Pullup' }});
  wrapper.find('input').at(1).simulate('change', { target: { value: '22' }});
  wrapper.find('input').at(2).simulate('change', { target: { value: '150' }});

  wrapper.find('form').simulate('submit', {
    preventDefault: () => {},
  });
  expect(handleFinishExerciseSpy).toHaveBeenLastCalledWith('Pullup', '22', '150');
});