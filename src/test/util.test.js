import React from 'react';
import { shallow } from 'enzyme';
import toJSON from 'enzyme-to-json';
import { Timer1, CountDownComponent } from '../utils';


test('should render Timer', () => {
  const start = '2019-06-05T00:00:00.000Z';
  const finish = '2019-06-05T00:11:22.333Z';

  jest.spyOn(global.Date, 'now')
    .mockImplementationOnce(() => new Date(finish));

  const wrapper = shallow(<Timer1 startTime={new Date(start)} />);
  expect(wrapper.props()['timer']['delay']).toBe(1000);
  expect(toJSON(wrapper)).toMatchSnapshot();
});

test('should shallow render CountDownComponent', () => {
  const start = '2019-06-05T00:00:00.000Z';
  const finish = '2019-06-05T00:11:22.333Z';

  jest.spyOn(global.Date, 'now')
    .mockImplementationOnce(() => new Date(finish));

  const wrapper = shallow(<CountDownComponent startTime={new Date(start)} />);
  expect(toJSON(wrapper)).toMatchSnapshot();
});

test('should shallow render CountDownComponent for larger duration', () => {
  const start = '2019-06-05T00:00:00.000Z';
  const finish = '2019-06-05T23:00:00.00Z';

  jest.spyOn(global.Date, 'now')
    .mockImplementationOnce(() => new Date(finish));

  const wrapper = shallow(<CountDownComponent startTime={new Date(start)} />);
  expect(toJSON(wrapper)).toMatchSnapshot();
});
