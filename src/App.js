import React from 'react';

import 'normalize.css/normalize.css';
import './styles/styles.scss';

import Header from './components/Header';
import WorkoutStates from './components/WorkoutStates';

function App() {
  return (
    <div className="App">
      <Header />
      <WorkoutStates />
    </div>
  );
}

export default App;
